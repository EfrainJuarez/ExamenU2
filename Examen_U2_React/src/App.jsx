import { useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'
import { Componente1 } from './assets/componente1/Componente1'
import { Componente2 } from './assets/componente2/Componente2'

function App() {
  const [count, setCount] = useState(0)

  return (
    <div className="App">
      <h1>Examen U2</h1>
      <div className="main_container">
      <Componente1></Componente1>
      <Componente2></Componente2>

      </div>
    </div>
  )
}

export default App
