import React from 'react'
import './componente2.css'
export const Componente2 = () => {
    const comandos= {
        comando1: "npm init vite@latest",
        comando2: "cd 'carpeta creada'",
        comando3: "npm install",
        comando4:"npm run dev",
        comando5: "npm run build"
    
        };
  return (
    <>
    <div className="main">
        <h2>Componente 2</h2>
    <div className="container">
        <h3>Creacion del proyecto</h3>
        <ol type='1'>
            <li>{comandos.comando1}</li>
            <li>{comandos.comando2}</li>
            <li>{comandos.comando3}</li>
        </ol>
        <h3>Levantar el servidor</h3>
        <ul>
            <li>{comandos.comando4}</li>
        </ul>
        <h3>Construir Proyecto</h3>
        <ul>
            <li>{comandos.comando5}</li>
        </ul>
    </div>
    </div>
    </>
  )
}
