import React from 'react'
import './comonente1.css'
export const Componente1 = () => {
    const datosP= {
    nombre: "Efrain",
    apellidos: "Juarez",
    edad: 21,
    carrer:"ISC",
    estado: "Soltero ):"

    };
  return (
    <>
    <div className="main">
    <h2>Componente 1</h2>
    <div className='container'>
        <p><strong>Nombre: </strong>{ datosP.nombre}</p>
        <p><strong>Apellidos: </strong>{datosP.apellidos}</p>
        <p><strong>Edad </strong>{datosP.edad}</p>
        <p><strong>Carrera: </strong>{datosP.carrer}</p>
        <p><strong>Estado civil: </strong>{datosP.estado}</p>
    </div>
    </div>
    </>
  )
}
